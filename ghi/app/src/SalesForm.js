import { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';

function SalesForm() {
    // SETTING STATE FOR DROPDOWN MENUS
    const[automobiles,setAutomobiles] = useState([]);
    const[salespeople, setSalesPeople] = useState([]);
    const[customers, setCustomers] = useState([]);

    // SETTING STATE FOR FORM DATA
    const[automobile, setAutomobile] = useState("");
    const[salesperson, setSalesPerson] = useState("");
    const[customer, setCustomer] = useState("");
    const[price,setPrice] = useState("");

    const navigate = useNavigate();

    async function getAutos() {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    async function getSalesPeople() {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople)
        }
    }

    async function getCustomers() {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        getAutos();
        getSalesPeople();
        getCustomers();
    },[]);

    async function handleSubmit(event) {
        event.preventDefault();

        const data = {
            automobile,
            salesperson,
            customer,
            price,
        };

        const url = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url,fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            setAutomobile("");
            setSalesPerson("");
            setCustomer("");
            setPrice("");

            navigate("/sales");
        }
    }

    function handleAutomobileChange (event) {
        const value = event.target.value;
        setAutomobile(value);
    }

    function handleSalesPersonChange (event) {
        const value = event.target.value;
        setSalesPerson(value);
    }

    function handleCustomerChange (event) {
        const value = event.target.value;
        setCustomer(value);
    }

    function handlePriceChange (event) {
        const value = event.target.value;
        setPrice(value);
    }


    return(
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Record a New Sale</h1>
            <form onSubmit={handleSubmit} id="create-Sales-form">

                <div className="mb-3">
                <select onChange={handleAutomobileChange} value={automobile.vin} required name="automobile" id="automobile" className="form-select">
                    <option value="">Choose a Automobile</option>
                    {automobiles
                    .filter(auto => {
                        return !auto.sold
                    })
                    .map(auto => {
                    return (
                        <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                    )
                    })}
                </select>
                </div>

                <div className="mb-3">
                <select onChange={handleSalesPersonChange} value={salesperson.id} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a Salesperson</option>
                    {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                    )
                    })}
                </select>
                </div>

                <div className="mb-3">
                <select onChange={handleCustomerChange} value={customer.id} required name="customer" id="customer" className="form-select">
                    <option value="">Choose a Customer</option>
                    {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                    )
                    })}
                </select>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
                </div>

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default SalesForm;
