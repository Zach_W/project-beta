import { useEffect, useState } from 'react';

function SalesPeopleHistoryList(){
    const[ salespeople, setSalesPeople] = useState([]);
    const[ sales, setSales] = useState([]);

    const[filteredSales, setFilteredSales] = useState([]);
    const[salesperson, setSalesPerson] = useState("");

    async function getSalesPeople() {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople)
        }
    }

    async function getSales() {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }


    useEffect(() => {
        getSalesPeople();
        getSales();
    },[]);


    function handleSalesPersonChange (event) {
        const value = event.target.value;
        setSalesPerson(value);
        setFilteredSales(sales.filter(sale => sale.salesperson.id === Number(value)));
    }

    return (
        <div className='mb-3'>
        <h2>Salesperson History</h2>
        <div className="mb-3">
        <select onChange={handleSalesPersonChange} value={salesperson.id} required name="salesperson" id="salesperson" className="form-select">
            <option value="">Choose a Salesperson</option>
            {salespeople.map(salesperson => {
            return (
                <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
            )
            })}
        </select>
        </div>

        <table  className='table table-striped'>
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>Vin</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            {filteredSales.map(sale => (
                <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
                </tr>
            ))}
            </tbody>
        </table>
        </div>
    );

}

export default SalesPeopleHistoryList;
