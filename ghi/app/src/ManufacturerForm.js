import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';


function ManufacturerForm() {
    const[ name , setName ] = useState("");
    const navigate = useNavigate();

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name,
        };


        const url = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url,fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            setName("");
            navigate("/manufacturers");
        }
    }

    function handleManufacturerChange (event) {
        const value = event.target.value;
        setName(value);
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a New Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-Manufacturer-form">

                <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={name} placeholder="Manufacturer Name.." required type="text" name="manufacturer_name" id="manufacturer_name" className="form-control" />
                <label htmlFor="manufacturer_name">Manufacturer</label>
                </div>

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default ManufacturerForm;
