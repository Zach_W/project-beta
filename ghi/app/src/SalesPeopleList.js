import { useEffect, useState } from 'react';



function SalesPeopleList(){
const[ salespeople, setSalesPeople] = useState([]);

useEffect(() => {
    async function getSalesPeople() {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople)
        }
    }
    getSalesPeople();
},[]);

return (
    <table  className='table table-striped'>
        <thead>
            <tr>
                <th>Employee Id</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
        </thead>
        <tbody>
        {salespeople.map((salesperson) => {
            return(
                <tr key={salesperson.id}>
                    <td>{salesperson.employee_id}</td>
                    <td>{salesperson.first_name}</td>
                    <td>{salesperson.last_name}</td>
                </tr>
            );
        })}
        </tbody>
    </table>
);

}
export default SalesPeopleList;
