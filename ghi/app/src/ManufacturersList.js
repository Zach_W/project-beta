import { useEffect, useState } from 'react';


function ManufacturersList() {

    // Set Manufacturer State
    const [manufacturers, setManufacturers] = useState([]);
    async function getManufacturers() {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        getManufacturers();
    },[]);

    return (
        <table  className='table table-striped'>
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
            {manufacturers.map((manufacturer) => {
                return(
                    <tr key={manufacturer.id}>
                        <td>{manufacturer.name}</td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default ManufacturersList;
