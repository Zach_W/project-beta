from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import SalesPerson, Customer, Sale, AutomobileVO

# Create your views here.
class AutomobileVOEncoder (ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]

class SalesPersonEncoder (ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class CustomerEncoder (ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SaleEncoder (ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET","POST"])
def api_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = SalesPerson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder = SalesPersonEncoder,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salespeople does not exist."},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder = SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson could not be created."},
                status=404,
            )


@require_http_methods(["DELETE"])
def api_show_salespeople(request,id):
    if request.method == "DELETE":
        try:
            salesperson = SalesPerson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                {"message": "Salesperson has been deleted."}
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message":"Salesperson could not be deleted."},
                status=404,
                )


@require_http_methods(["GET","POST"])
def api_customer(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder = CustomerEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customers do not exist."},
                status=404,
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer could not be created."},
                status=404,
            )


@require_http_methods(["DELETE"])
def api_show_customer(request,id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                {"message": "Customer has been deleted."}
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message":"Customer could not be deleted."},
                status=404,
                )


@require_http_methods(["GET","POST"])
def api_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder = SaleEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales does not exist."},
                status = 404,
            )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin = automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist."},
                status = 404,
            )
        try:
            salesperson_id = content["salesperson"]
            salesperson = SalesPerson.objects.get(id = salesperson_id)
            content["salesperson"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist."},
                status = 404,
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id = customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist."},
                status = 404,
            )
        try:
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder = SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson could not be created."},
                status=404,
            )


@require_http_methods(["DELETE"])
def api_show_sale(request,id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                {"message": "Sale has been deleted."}
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message":"Sale could not be deleted."},
                status=404,
                )
