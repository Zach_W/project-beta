from django.urls import path
from .views import (
    api_salespeople,
    api_show_salespeople,
    api_customer,
    api_show_customer,
    api_sales,
    api_show_sale,
    )

urlpatterns = [
    path("salespeople/",api_salespeople, name="api_salespeople"),
    path("salespeople/<int:id>/",api_show_salespeople, name="api_show_salespeople"),
    path("customers/",api_customer, name="api_customers"),
    path("customers/<int:id>/",api_show_customer,name="api_show_customer"),
    path("sales/",api_sales, name= "api_sales"),
    path("sales/<int:id>/",api_show_sale, name="api_show_sale"),
]
